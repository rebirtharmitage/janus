package main

import (
		"io/ioutil"
		"net/http"
		"html/template"
		"log"
		"gopkg.in/mgo.v2"
		"gopkg.in/mgo.v2/bson"
)

type Page struct {
		Title string
		Body  []byte
}

type Person struct {
		Name string
        Phone string
}

func loadPage(title string) (*Page, error){
        session, err := mgo.Dial("LOCALHOST:27017")
        if err != nil {
                panic(err)
        }
        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        c := session.DB("test").C("people")

        result := Person{}
        err = c.Find(bson.M{"name": title}).One(&result)
        if err != nil {
                log.Fatal(err)
        }
		
        return &Page{Title: title, Body: []byte(result.Phone)}, nil
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

/* func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
} */

func viewHandler(w http.ResponseWriter, r *http.Request) {
    title := r.URL.Path[len("/view/"):]
    p, _ := loadPage(title)
    t, _ := template.ParseFiles("view.html")
    t.Execute(w, p)
}

func main() {
	http.HandleFunc("/view/", viewHandler)
	http.ListenAndServe(":8080", nil)
}

